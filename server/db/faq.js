const db = require('./db');
const logger = require('../util/logger');


function getFaqs(req, res, next) {
  logger.winston.info('faq.getFaqs');

  db.all('select * from faq where bot_id = ? order by faq_id desc', req.query.bot_id, function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}

function getFaqListPage(req, res, next) {
  logger.winston.info('faq.getFaqListPage');
  const len = req.query.size ? req.query.size : 10;
  const start = req.query.start ? req.query.start : 0;

  db.all('select * from faq where bot_id = ? order by faq_id asc limit ?, ?', [req.query.bot_id, start, len], function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}

function createFaq(req, res, next) {
  logger.winston.info('faq.createFaq');
  db.run('insert into faq(bot_id, question)' + 'values (?,?)', [req.body.bot_id, req.body.question], function(err) {
    if (err) {
      logger.winston.error("Error inserting a new record");
    } else {
      db.get('SELECT last_insert_rowid()', function(err, data) {
        if (err) {
          res.status(500).json({ status: 'error', message: '' });
        } else {
          res.status(200).json({ status: 'success', message: 'Inserted', faq_id: data['last_insert_rowid()'] });
        }
      });
    }
  });
}

function updateFaq(req, res, next) {
  logger.winston.info('faq.updateFaq');
  db.run('update faq set question = ? where faq_id = ?', [req.body.question, req.body.faq_id], function(err) {
    if (err) {
      logger.winston.error("Error updating the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Updated' });
    }
  });
}

function removeFaq(req, res, next) {
  logger.winston.info('faq.removeFaq');
  const botId = req.query.bot_id;
  const faqId = req.query.faq_id;
  if (botId) {
    db.run("delete from faq where bot_id = ?", botId, function (err) {
      if (err){
        logger.winston.error("Error removing the record!!!");
      } else {
        res.status(200).json({ status: "success", message: 'Removed'});
      }
    })
  } else {
    db.run("delete from faq where faq_id = ?", faqId, function (err) {
      if (err){
        logger.winston.error("Error removing the record!!!");
      } else {
        res.status(200).json({ status: "success", message: 'Removed'});
      }
    })
  }
}

module.exports = {
  getFaqs,
  getFaqListPage,
  createFaq,
  updateFaq,
  removeFaq
};
