const db = require('./db');
const logger = require('../util/logger');

function getSingleLookupTable(req, res, next) {
  logger.winston.info('lookupTables.getSingleLookupTable');
  db.get('select * from lookup_tables where lookup_table_id = ?', req.params.lookup_table_id, function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}

function getLookupTables(req, res, next) {
  logger.winston.info('lookupTables.getLookupTables');
  db.all('select * from lookup_tables where lookup_id = ? order by lookup_table_id desc', req.params.lookup_id, function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}

function getLookupsTables(req, res, next) {
  logger.winston.info('lookupTables.getLookupsTables');
  const lookupId = req.params.lookup_id;
  var array_lookupIds = lookupId.split(","); //Very hacky due to the node-sqlite not supporting IN from an array
  db.all('select * from lookup_tables where lookup_id in (' + array_lookupIds + ')', function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}

function createLookupTable(req, res, next) {
  logger.winston.info('lookupTables.createLookupTable');
  db.run('insert into lookup_tables (lookup_id, lookup_value)' + 'values (?, ?)', [req.body.lookup_id, req.body.lookup_value], function(err) {
    if (err) {
      logger.winston.error("Error inserting a new record");
    } else {
      db.get('SELECT last_insert_rowid()', function(err, data) {
        if (err) {
          res.status(500).json({ status: 'error', message: '' });
        } else {
          res.status(200).json({ status: 'success', message: 'Inserted', lookup_table_id: data['last_insert_rowid()'] });
        }
      });
    }
  });
}

function removeSingleLookupTable(req, res, next) {
  logger.winston.info('lookupTables.removeSingleLookupTable');
  db.run('delete from lookup_tables where lookup_table_id = ?', req.params.lookup_table_id, function(err) {
    if (err) {
      logger.winston.error("Error removing the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Removed' });
    }
  });
}

function removeLookupTables(req, res, next) {
  logger.winston.info('lookupTables.removeLookupTables');
  db.run('delete from lookup_tables where lookup_id = ?', req.params.lookup_id, function(err) {
    if (err) {
      logger.winston.error("Error removing the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Removed' });
    }
  });
}

module.exports = {
  getSingleLookupTable,
  getLookupTables,
  createLookupTable,
  removeSingleLookupTable,
  removeLookupTables,
  getLookupsTables
};
