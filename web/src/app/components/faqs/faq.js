angular.module('app').controller('FaqController', FaqController);

function FaqController($rootScope, $scope, Bot, Faq, $window) {
  $scope.message = {};
  $scope.message.text = "";
  $scope.bot = {};
  $scope.selectedBot = {};

  $scope.question = "";

  Bot.query(function (data) {
    $scope.botList = data;

    if ($scope.$routeParams.bot_id) {
      $scope.selectedBot = $scope.objectFindByKey($scope.botList, 'bot_id', Number($scope.$routeParams.bot_id));
      $scope.bot.bot_id = $scope.selectedBot.bot_id;
      $scope.loadFaq($scope.selectedBot.bot_id);
    }
  });

  // faq操作 start
  $scope.saveFaq = function(question) {
    Faq.save({bot_id: $scope.selectedBot.bot_id, question: question}).$promise.then(function() {
      if (window.location.hash === '#/faq/' + $scope.selectedBot.bot_id)
        $scope.reloadRoute();
      else
        $scope.go('/faq/' +  $scope.selectedBot.bot_id);

      // $rootScope.$broadcast('setAlertText', 'Faq saved!!!');
      $rootScope.$broadcast('setAlertText', '常见问题已保存！！！');
    });
  };

  $scope.updateFaq = function(faq_id, question) {
    Faq.update({ faq_id: faq_id, question: question}).$promise.then(function() {
      $scope.go('/faq/' +  $scope.selectedBot.bot_id);
      // $rootScope.$broadcast('setAlertText', 'Faq updated!!!');
      $rootScope.$broadcast('setAlertText', '常见问题已更新！！！');
    });
  };

  $scope.deleteFaq = function(faq_id) {
    Faq.delete({faq_id: faq_id }, data => {
      $scope.loadFaq($scope.selectedBot.bot_id);
    });
  };
  // faq操作 end


  // 刷新页面数据
  $scope.loadFaq = function(bot_id) {
    $scope.selectedBot = $scope.objectFindByKey($scope.botList, 'bot_id', bot_id);

    Faq.query({ bot_id: bot_id }, data => {
      $scope.faqList = data;
    });
  };

  $scope.reloadRoute = function () {
    $window.location.reload();
  };
}
